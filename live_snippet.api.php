<?php
/**
 * Implementation of hook_live_snippet_editors
 * 
 * {module}_live_snippet_editors
 * 
 * @return array - an array of arrays keyed by the editors machine name and containing a title, link and description
 */
 
function hook_live_snippet_editors(){
	return array(
		'ace' => array(
			'title' => t('Ace'),
			'link' => 'http://ace.c9.io/',
			'description' => t('Ace is an embeddable code editor written in JavaScript. It matches the features and performance of native editors such as Sublime, Vim and TextMate. It can be easily embedded in any web page and JavaScript application.'),
		)
	);
}

/**
 * Implementation of hook_editor_view
 * 
 * {editor}_live_snippet_editors
 * 
 * @return array - an array ready for render, live_snippet handles multi valued fields so only the current item is sent in here
 */
function hook_editor_view($entity_type, $entity, $field, $instance, $langcode, $item, $display){
	//Initialize the element
	$element=array();
	
	//Get a unique id for the editor
	$id='my-editor-'.live_snippet_instance_id();
	
	//Construct and return the renderable array
	return array(
		'#markup' => '<div class="editor" id="'.$id.'">'.$item['snippet'].'</div>',
	);
}

/**
 * Implementation of hook_editor_widget
 * 
 * {editor}_live_snippet_editors
 * 
 * @return an array ready for theming by the form api, a field called "snippet" must be added.
 */
 
function hook_editor_widget($form, $form_state, $field, $instance, $langcode, $items, $delta, $element){
	return array(
		'snippet' => array(
			'#type' => 'textarea',
			'#title' => 'Snippet',
			'#default_value' => $items[$delta]['snippet'],
		)
	);
}