(function($){
	Drupal.settings.ace_editor={
		editors:[]
	};
	ace_initialize=function(context, settings){
		$(".field-type-live-snippet .ace-editor:not(.ace-processed)", context).each(function(){
			$(this).addClass('ace-processed');
			var wrapper=$(this).parent();
			
			var textarea=wrapper.find('.form-textarea');
			
			var settings=$(this).data();
			var editor = ace.edit(this);
			
			editor.setReadOnly(settings.read_only);
		    editor.setTheme("ace/theme/"+settings.theme);
		    editor.getSession().setMode("ace/mode/"+settings.mode);
		    
		    $(this).find('.ace-mode').change(function(){
		    	editor.getSession().setMode('ace/mode/'+$(this).val());
		    });
		    $(this).find('.ace-theme').change(function(){
		    	editor.setTheme('ace/theme/'+$(this).val());
		    });
		    if(settings.readonly!==true){
			    editor.on('change', function(e){
			    	textarea.text(editor.getValue());
			    });
		    }
		});
	}
	$(document).ready(function(){
		Drupal.behaviors.live_snippet_ace={
			attach: ace_initialize,
		}
		ace_initialize();
	});
	
})(jQuery)
